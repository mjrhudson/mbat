#                             MBAT
#               Copyright (C) 2021  Matt Hudson
#                   Email: matt@mjrhudson.uk

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

###########################################################################
#           TITLE: mbatcli.py                                             #
#         AUTHORS: Matt Hudson (matt@mjrhudson.uk)                        #
# INITIAL RELEASE: 23-November-2020                                       #
#   LAST MODIFIED: 07-February-2021                                       #
###########################################################################

# SECTION Import Libraries
import sys
import os
import platform
import getpass
import time
import json
import hashlib
import pathlib
import datetime
import logging
import lib.libmbatcli as libmbatcli
import lib.libmbatcore as libmbatcore
# !SECTION Import Libraries

class mbatcli:
    def __init__(self,sysargsin):
        # Base Parameters
        self.status = 2
        self.rootpath = str(pathlib.Path(__file__).parent.absolute())
        # Set log filename / path and start logger
        logtimestamp = datetime.datetime.today()
        logdate = logtimestamp.strftime("%Y") + logtimestamp.strftime("%m") + logtimestamp.strftime("%d")
        logtime = logtimestamp.strftime("%H") + logtimestamp.strftime("%M") + logtimestamp.strftime("%S")
        self.logfilepath = self.rootpath + os.path.sep + "logs" + os.path.sep + logdate + "T" + logtime + "_MBAT_log.txt"
        logging.basicConfig(filename=self.logfilepath,filemode="w",level=logging.DEBUG,format="%(asctime)s %(levelname)s %(message)s",datefmt="%Y-%m-%d %H:%M:%S")
        # Create base archive properties for class
        self.modeFlag = ""
        self.inputpaths = []
        self.outputpath = ""
        self.numArchives = 0
        self.profilename = ""
        self.libEnc = ""
        self.password = ""
        self.archiveName = ""
        # Parse system arguments using sysargsParser
        sysargsp = sysargsParser(sysargsin)
        # Set base archive parameters parsed from system arguments
        self.modeFlag = sysargsp.modeFlag
        self.libEnc = sysargsp.libEnc
        self.useMasterPass = sysargsp.useMasterPass
        self.masterPass = sysargsp.masterPass
        self.archiveName = sysargsp.archiveName
        # Load default settings from settings-*.json (currently just self.defaultoutputpath)
        self.defaultoutputpath = libmbatcli.loadSettings(self.rootpath)
        # Load profile and set profile properties if using profile mode
        if "p" in self.modeFlag:
            profile = libmbatcli.loadProfile(self.rootpath,sysargsp.modeInput[0])
            self.profilename = profile["name"]
            self.inputpaths = profile["dirs"]
            if not profile["outputpath"] == "":
                self.defaultoutputpath = profile["outputpath"]
        else:
            self.inputpaths = sysargsp.modeInput
        # Check input paths provided are valid and clean up if necessary
        for i in range(len(self.inputpaths)):
            inputpath = self.inputpaths[i]
            # Remove any trailing file separators from input path
            while inputpath.endswith("\"") or inputpath.endswith("\\") or inputpath.endswith("/"):
                inputpath = inputpath[:-1]
            # Check if input path is empty
            if inputpath=="":
                logging.error("Input path is empty")
                print("\nError: Input path is empty.\n")
                exit()
            # Write changes (if any) back into self.inputpaths
            self.inputpaths[i] = inputpath
        # Check that all input paths provided are directories when in creation mode (single file mode currently not implemented)
        if "c" in self.modeFlag:
            for i in range(len(self.inputpaths)):
                if not os.path.isdir(self.inputpaths[i]):
                    logging.error("Input paths must be folders for archive creation")
                    logging.error("\"%s\" does not lead to a folder",self.inputpaths[i])
                    print("\nError: Input paths must be folders for archive creation.\n\n\"" + self.inputpaths[i] + "\" does not lead to a folder.\n")
                    exit()
        # Get filepaths for any MBAT archives within any input path directories
        # This is only relevant when extracting or verifying alone (i.e not verifying as part of archive creation)
        elif "x" in self.modeFlag or ("v" in self.modeFlag and not "c" in self.modeFlag):
            self.inputpaths = libmbatcore.getMbatFilepaths(self.inputpaths)[0]
        # Assign output path to default if one hasn't been provided by either the user or the profile in use
        if sysargsp.modeOutput == "$DEFAULTOUTPUT":
            self.outputpath = self.defaultoutputpath
        else:
            self.outputpath = sysargsp.modeOutput
        # Check that the output path exists; attempt to create if it doesn't
        if not os.path.isdir(self.outputpath):
            os.mkdir(self.outputpath)
            if not os.path.isdir(self.outputpath):
                logging.error("Failed to create output path")
                print("\nError: Failed to create output path.\n")
                exit()
        # Set the number of archives to be created/extracted/verified
        self.numArchives = len(self.inputpaths)
        # Check custom archive name is only be used in single archive mode
        if sysargsp.customArchiveName and self.numArchives > 1:
            logging.error("Only 1 archive can be created when using a custom archive name")
            print("\nError: Only 1 archive can be created when using a custom archive name.\n")
            exit()
        # Get the versions of any libraries installed on the system PATH that might be used (namely, GnuPG and OpenSSL)
        self.libVersions = libmbatcli.getLibVersions()
    # SECTION Define Functions
    def run(self):
        # Request input from user if master password is in use (and not provided via command line)
        if self.useMasterPass:
            if self.masterPass == "":
                print("")
                self.password = libmbatcli.getPassword(True)
            else:
                self.password = self.masterPass
                print("")
            # Hash password to both protect it in memory and provide an extra step of entropy
            self.password = hashlib.sha1(self.password.encode("utf-8")).hexdigest()
        else:
            print("")
        tOverallStart = time.perf_counter()
        # Get core MBAT version and create archive settings dict
        # archiveSettings is used to store all of the settings relevant to the current archive
        # It is passed by value to the core MBAT functions via json.loads(json.dumps()) as
        # this is thread safe, whereas copy.deepcopy() is not
        mbatCoreVersion = libmbatcore.getVersionInfo()
        archivesettings = {
            "mbatversion": mbatCoreVersion,
            "archivename": "",
            "outputpath": self.outputpath,
            "archivefilepath": "",
            "targetpath": "",
            "modeFlag": self.modeFlag,
            "libEnc": self.libEnc,
            "libEncVersion": self.libVersions[self.libEnc]["version"],
            "password": self.password,
            "writeVerificationLog": False
        }
        overallSuccess = True # Tracks whether there have been any errors throughout the entire process
        # Loop through each archive in self.inputspaths
        for iArchive in range(self.numArchives):
            tStart = time.perf_counter()

            inputpath = self.inputpaths[iArchive]

            # Determine which process to use: Creation, extraction, (standalone) verification or displaying archive information
            if "c" in self.modeFlag:
                archivesettings["archivename"] = self.archiveName
                archivesettings["targetpath"] = inputpath
                if "e" in archivesettings["modeFlag"] and archivesettings["password"] == "":
                    archivesettings["password"] = libmbatcli.getPassword(True)
                    archivesettings["password"] = hashlib.sha1(archivesettings["password"].encode("utf-8")).hexdigest()
                archivesettings["archivefilepath"], status = libmbatcore.createArchive(json.loads(json.dumps(archivesettings)))
                if status == 0:
                    overallSuccess = False
                elif "v" in self.modeFlag:
                    status = libmbatcore.verifyArchive(json.loads(json.dumps(archivesettings)))
                    if status == 0:
                        overallSuccess = False
                if not self.useMasterPass:
                    archivesettings["password"] = ""
            elif "x" in self.modeFlag:
                archivesettings["archivefilepath"] = inputpath
                status, manifest = libmbatcore.getArchiveInfo(json.loads(json.dumps(archivesettings)))
                if status == 0:
                    overallSuccess = False
                else:
                    if "e" in manifest["info"][3] and archivesettings["password"] == "":
                        archivesettings["password"] = libmbatcli.getPassword(False)
                        archivesettings["password"] = hashlib.sha1(archivesettings["password"].encode("utf-8")).hexdigest()
                    status = libmbatcore.extractArchive(json.loads(json.dumps(archivesettings)))
                    if status == 0:
                        overallSuccess = False
                if not self.useMasterPass:
                    archivesettings["password"] = ""
            elif "v" in self.modeFlag:
                archivesettings["archivefilepath"] = inputpath
                status, manifest = libmbatcore.getArchiveInfo(json.loads(json.dumps(archivesettings)))
                if status == 0:
                    overallSuccess = False
                else:
                    if "e" in manifest["info"][3] and archivesettings["password"] == "":
                        archivesettings["password"] = libmbatcli.getPassword(False)
                        archivesettings["password"] = hashlib.sha1(archivesettings["password"].encode("utf-8")).hexdigest()
                    status = libmbatcore.verifyArchive(json.loads(json.dumps(archivesettings)))
                    if status == 0:
                        overallSuccess = False
                if not self.useMasterPass:
                    archivesettings["password"] = ""
            elif "i" in self.modeFlag:
                archivesettings["archivefilepath"] = inputpath
                status, manifest = libmbatcore.getArchiveInfo(json.loads(json.dumps(archivesettings)))
                if status == 0:
                    overallSuccess = False
                else:
                    libmbatcli.printManifestInfo(archivesettings["archivefilepath"],manifest)
            
            tEnd = time.perf_counter()
            if not "i" in self.modeFlag:
                tStr = time.strftime("%H hours %M minutes %S seconds",time.gmtime(tEnd-tStart))
                logging.info("Completed in %s",tStr)
                print("Completed in " + tStr + "\n")
        if overallSuccess:
            self.status = 1
        else:
            self.status = 0
        tOverallEnd = time.perf_counter()
        if not "i" in self.modeFlag:
            tOverallStr = time.strftime("%H hours %M minutes %S seconds",time.gmtime(tOverallEnd-tOverallStart))
            if self.status == 1:
                print("---------------------------------------------------------------------------\n")
                if "p" in self.modeFlag:
                    logging.info("Completed \"%s\" profile with no errors in %s",self.profilename,tOverallStr)
                    print("Completed \"" + self.profilename + "\" profile with no errors in " + tOverallStr + "\n")
                elif self.numArchives > 1:
                    logging.info("Completed all archives with no errors in %s",tOverallStr)
                    print("Completed all archives with no errors in " + tOverallStr + "\n")
                else:
                    logging.info("Completed archive with no errors in %s",tOverallStr)
                    print("Completed archive with no errors in " + tOverallStr + "\n")
            else:
                print("---------------------------------------------------------------------------\n")
                if "p" in self.modeFlag:
                    logging.warning("Completed \"%s\" profile with errors in %s",self.profilename,tOverallStr)
                    print("Completed \"" + self.profilename + "\" profile with errors in " + tOverallStr + "\n")
                elif self.numArchives > 1:
                    logging.warning("Completed all archives with errors in %s",tOverallStr)
                    print("Completed all archives with errors in " + tOverallStr + "\n")
                else:
                    logging.warning("Completed archive with errors in %s",tOverallStr)
                    print("Completed archive with errors in " + tOverallStr + "\n")
    # !SECTION Define Functions

class sysargsParser:
    def __init__(self,sysargsin):
        # Parsed
        self.modeFlag = ""
        self.modeInput = []
        self.modeOutput = "$DEFAULTOUTPUT"
        self.libEnc = "gpg"
        self.useMasterPass = False
        self.masterPass = ""
        self.customArchiveName = False
        self.archiveName = "%Y%m%dT%H%M%SMBAT-$TARGETNAME"
        # Function call
        self.handleSysArgs(sysargsin)
    # SECTION Define Functions
    def handleSysArgs(self,sysargsin):
        if len(sysargsin) == 2:
            if sysargsin[1] == "-h" or sysargsin[1] == "--help":
                libmbatcli.printHelpInfo()
                exit()
        elif len(sysargsin) < 3:
            logging.error("Invalid number of inputs passed")
            print("\nError: Invalid number of inputs passed.\n")
            exit()
        self.handleFlags(sysargsin[1:])
        if len(self.modeInput) == 0:
            logging.error("No input paths or profile name entered")
            print("\nError: No input paths or profile name entered.\n")
            exit()
        while self.modeOutput.endswith("\'") or self.modeOutput.endswith("\"") or self.modeOutput.endswith("\\") or self.modeOutput.endswith("/"):
            self.modeOutput = self.modeOutput[:-1]
        if self.modeOutput=="":
            logging.error("No output path entered")
            print("\nError: No output path entered.\n")
            exit()
    
    def handleFlags(self,inputargs):
        inputsEntered = False
        i = 0
        while i < len(inputargs):
            inputarg = inputargs[i]
            if inputarg.startswith("--"):
                if inputarg == "--create":
                    if not "c" in self.modeFlag:
                        self.modeFlag = self.modeFlag + "c"
                    i += 1
                elif inputarg == "--extract":
                    if not "x" in self.modeFlag:
                        self.modeFlag = self.modeFlag + "x"
                    i += 1
                elif inputarg == "--verify":
                    if not "v" in self.modeFlag:
                        self.modeFlag = self.modeFlag + "v"
                    i += 1
                elif inputarg == "--encrypt":
                    if not "e" in self.modeFlag:
                        self.modeFlag = self.modeFlag + "e"
                    if i == len(inputargs)-1:
                        i += 1
                    else:
                        if inputargs[i+1].startswith("--"):
                            i += 1
                        else:
                            self.libEnc = inputargs[i+1]
                            i += 2
                elif inputarg == "--compress":
                    if not "z" in self.modeFlag:
                        self.modeFlag = self.modeFlag + "z"
                    i += 1
                elif inputarg == "--info":
                    if not "i" in self.modeFlag:
                        self.modeFlag = self.modeFlag + "i"
                    i += 1
                elif inputarg == "--profile":
                    if not "p" in self.modeFlag:
                        self.modeFlag = self.modeFlag + "p"
                    i += 1
                elif inputarg == "--help":
                    if not "h" in self.modeFlag:
                        self.modeFlag = self.modeFlag + "h"
                    i += 1
                elif inputarg == "--masterpass":
                    if i == len(inputargs)-1:
                        self.useMasterPass = True
                        self.masterPass = ""
                        i += 1
                    else:
                        if inputargs[i+1].startswith("--"):
                            self.useMasterPass = True
                            self.masterPass = ""
                            i += 1
                        else:
                            self.useMasterPass = True
                            self.masterPass = inputargs[i+1]
                            i += 2
                elif inputarg == "--archivename":
                    self.archiveName = inputargs[i+1]
                    self.customArchiveName = True
                    i += 2
                elif inputarg == "--in" or inputarg == "--input":
                    if inputsEntered:
                        logging.error("Extra input arguments can't be passed using \"--in\" or \"--input\" in addition to those passed after short flags")
                        print("\nError: Extra input arguments can't be passed using \"--in\" or \"--input\" in addition to those passed after short flags.\n")
                        exit()
                    else:
                        inputsEntered = True
                        if i + 2 == len(inputargs):
                            inputpaths = [inputargs[i+1]]
                        else:
                            nextArgPos = i
                            for j in range(i + 2,len(inputargs),1):
                                if inputargs[j].startswith("--"):
                                    nextArgPos = j
                                    break
                            if nextArgPos == i:
                                inputpaths = inputargs[i+1:]
                            else:
                                inputpaths = inputargs[i+1:nextArgPos]
                        i = i + 1 + len(inputpaths)
                        self.modeInput = self.modeInput + inputpaths
                elif inputarg == "--out" or inputarg == "--output":
                    self.modeOutput = inputargs[i+1]
                    i += 2
                else:
                    logging.error("Invalid input")
                    print("\nError: Invalid input\n")
                    exit()
            elif inputarg.startswith("-"):
                inputarg = inputarg[1:] # Remove leading dash
                if "c" in inputarg:
                    self.modeFlag = self.modeFlag + "c"
                    inputarg = inputarg.replace("c","")
                if "v" in inputarg:
                    self.modeFlag = self.modeFlag + "v"
                    inputarg = inputarg.replace("v","")
                if "e" in inputarg:
                    self.modeFlag = self.modeFlag + "e"
                    inputarg = inputarg.replace("e","")
                if "z" in inputarg:
                    self.modeFlag = self.modeFlag + "z"
                    inputarg = inputarg.replace("z","")
                if "p" in inputarg:
                    self.modeFlag = self.modeFlag + "p"
                    inputarg = inputarg.replace("p","")
                if "x" in inputarg:
                    self.modeFlag = self.modeFlag + "x"
                    inputarg = inputarg.replace("x","")
                if "i" in inputarg:
                    self.modeFlag = self.modeFlag + "i"
                    inputarg = inputarg.replace("i","")
                if "h" in inputarg:
                    self.modeFlag = self.modeFlag + "h"
                    inputarg = inputarg.replace("h","")
                if not inputarg == "":
                    logging.error("Invalid short flags passed: \'%s\'","\', \'".join([char for char in inputarg]))
                    print("\nError: Invalid short flags passed: \'" + "\', \'".join([char for char in inputarg]) + "\'.\n")
                    exit()
                if self.modeFlag == "":
                    logging.error("No valid short flags passed")
                    print("\nError: No valid short flags passed.\n")
                    exit()
                i += 1
            elif i == 1:
                # Special case if this is the second input (if no flag is present, this is assumed to be either input paths or a profile name)
                if i + 1 == len(inputargs):
                    inputpaths = [inputarg]
                else:
                    nextArgPos = i
                    for j in range(i + 1,len(inputargs),1):
                        if inputargs[j].startswith("-"):
                            nextArgPos = j
                            break
                    if nextArgPos == i:
                        inputpaths = inputargs[i:]
                    else:
                        inputpaths = inputargs[i:nextArgPos]
                inputsEntered = True
                self.modeInput = inputpaths
                i = i + len(inputpaths)
            else:
                logging.error("\"%s\" is not a valid flag",inputarg)
                print("\nError: \"" + inputarg + "\"is not a valid flag.\n")
                exit()
            if "h" in self.modeFlag and not "h" == self.modeFlag:
                logging.error("It is not possible to combine the help (\"-h\" or \"--help\") flag with any other flags")
                print("\nError: It is not possible to combine the help (\"-h\" or \"--help\") flag with any other flags.\n")
                exit()
            if "p" in self.modeFlag:
                if len(self.modeInput) > 1:
                    logging.error("Multiple profiles are not yet supported")
                    print("\nError: Multiple profiles are not yet supported.\n")
                    exit()
            if "x" in self.modeFlag:
                if "c" in self.modeFlag:
                    logging.error("It is not possible to combine the extract (\"-x\" or \"--extract\") flag with the create (\"-c\" or \"--create\") flag")
                    print("\nError: It is not possible to combine the extract (\"-x\" or \"--extract\") flag with the create (\"-c\" or \"--create\") flag.\n")
                    exit()
                elif "p" in self.modeFlag:
                    logging.error("It is not possible to combine the extract (\"-x\" or \"--extract\") flag with the profile (\"-p\" or \"--profile\") flag")
                    print("\nError: It is not possible to combine the extract (\"-x\" or \"--extract\") flag with the profile (\"-p\" or \"--profile\") flag.\n")
                    exit()
                else:
                    self.modeFlag = self.modeFlag.replace("e","") # Encryption will be autodetected when extracting and so this flag can be removed
                    self.modeFlag = self.modeFlag.replace("z","") # Compression will be autodetected when extracting and so this flag can be removed
            if "i" in self.modeFlag and not self.modeFlag == "i":
                logging.error("It is not possible to combine the information (\"-i\" or \"--info\") flag with any other flags")
                print("\nError: It is not possible to combine the information (\"-i\" or \"--info\") flag with any other flags.\n")
                exit()
    # !SECTION Define Functions

# SECTION Main
mbatcli(sys.argv).run()
# !SECTION Main
