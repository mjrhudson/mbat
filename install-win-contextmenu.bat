@echo off

REM                             MBAT
REM               Copyright (C) 2021  Matt Hudson
REM                   Email: matt@mjrhudson.uk

REM This program is free software: you can redistribute it and/or modify
REM it under the terms of the GNU General Public License as published by
REM the Free Software Foundation, either version 3 of the License, or
REM (at your option) any later version.

REM This program is distributed in the hope that it will be useful,
REM but WITHOUT ANY WARRANTY; without even the implied warranty of
REM MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
REM GNU General Public License for more details.

REM You should have received a copy of the GNU General Public License
REM along with this program.  If not, see <http://www.gnu.org/licenses/>.

REM #######################################################################
REM #           TITLE: install-win-contextmenu.bat                        #
REM #         AUTHORS: Matt Hudson (matt@mjrhudson.uk)                    #
REM # INITIAL RELEASE: 04-April-2021                                      #
REM #   LAST MODIFIED: 05-April-2021                                      #
REM #######################################################################

REM Elevate script
if not "%1"=="elevated" (powershell start -Verb RunAs "%0" "elevated" & exit /B)

REM Display header
echo.
echo ***************** MBAT Windows Context Menu Script v0.1.1 *****************
echo.

REM Create keys and values for command definitions
echo|set /p="Creating command definitions..."
REG ADD "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\CommandStore\shell\mbat.create" /f /ve /t REG_SZ /d "Create archive" > nul 2>&1
REG ADD "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\CommandStore\shell\mbat.create-encrypt" /f /ve /t REG_SZ /d "Create and encrypt archive" > nul 2>&1
REG ADD "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\CommandStore\shell\mbat.create-encrypt-verify" /f /ve /t REG_SZ /d "Create, encrypt and verify archive" > nul 2>&1
REG ADD "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\CommandStore\shell\mbat.create-verify" /f /ve /t REG_SZ /d "Create and verify archive" > nul 2>&1
REG ADD "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\CommandStore\shell\mbat.extract" /f /ve /t REG_SZ /d "Extract archive" > nul 2>&1
REG ADD "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\CommandStore\shell\mbat.extract-verify" /f /ve /t REG_SZ /d "Extract and verify archive" > nul 2>&1
REG ADD "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\CommandStore\shell\mbat.showinfo" /f /ve /t REG_SZ /d "Show archive information" > nul 2>&1
REG ADD "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\CommandStore\shell\mbat.verify" /f /ve /t REG_SZ /d "Verify archive" > nul 2>&1
echo Done

REM Create subkeys and values for commands
echo|set /p="Creating command values..."
REG ADD "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\CommandStore\shell\mbat.create\command" /f /ve /t REG_SZ /d "\"%USERPROFILE%\.local\scripts\mbat\shell\mbat.bat\" \"-c\" \"--out\" \".\" \"--in\" \"%%1\"" > nul 2>&1
REG ADD "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\CommandStore\shell\mbat.create-encrypt\command" /f /ve /t REG_SZ /d "\"%USERPROFILE%\.local\scripts\mbat\shell\mbat.bat\" \"-ce\" \"--out\" \".\" \"--in\" \"%%1\"" > nul 2>&1
REG ADD "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\CommandStore\shell\mbat.create-encrypt-verify\command" /f /ve /t REG_SZ /d "\"%USERPROFILE%\.local\scripts\mbat\shell\mbat.bat\" \"-cve\" \"--out\" \".\" \"--in\" \"%%1\"" > nul 2>&1
REG ADD "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\CommandStore\shell\mbat.create-verify\command" /f /ve /t REG_SZ /d "\"%USERPROFILE%\.local\scripts\mbat\shell\mbat.bat\" \"-cv\" \"--out\" \".\" \"--in\" \"%%1\"" > nul 2>&1
REG ADD "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\CommandStore\shell\mbat.extract\command" /f /ve /t REG_SZ /d "\"%USERPROFILE%\.local\scripts\mbat\shell\mbat.bat\" \"-x\" \"--out\" \".\" \"--in\" \"%%1\"" > nul 2>&1
REG ADD "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\CommandStore\shell\mbat.extract-verify\command" /f /ve /t REG_SZ /d "\"%USERPROFILE%\.local\scripts\mbat\shell\mbat.bat\" \"-xv\" \"--out\" \".\" \"--in\" \"%%1\"" > nul 2>&1
REG ADD "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\CommandStore\shell\mbat.showinfo\command" /f /ve /t REG_SZ /d "\"%USERPROFILE%\.local\scripts\mbat\shell\mbat.bat\" \"-i\" \"--in\" \"%%1\"" > nul 2>&1
REG ADD "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\CommandStore\shell\mbat.verify\command" /f /ve /t REG_SZ /d "\"%USERPROFILE%\.local\scripts\mbat\shell\mbat.bat\" \"-v\" \"--in\" \"%%1\"" > nul 2>&1
echo Done

REM Create keys and values for file context menu
echo|set /p="Creating file context menu..."
REG ADD "HKEY_CLASSES_ROOT\*\shell\mbat" /f /v "MUIVerb" /t REG_SZ /d "MBAT" > nul 2>&1
REG ADD "HKEY_CLASSES_ROOT\*\shell\mbat" /f /v "SubCommands" /t REG_SZ /d "mbat.extract;mbat.extract-verify;mbat.verify;mbat.showinfo" > nul 2>&1
echo Done

REM Create keys and values for directory context menu
echo|set /p="Creating directory context menu..."
REG ADD "HKEY_CLASSES_ROOT\Directory\shell\mbat" /f /v "MUIVerb" /t REG_SZ /d "MBAT" > nul 2>&1
REG ADD "HKEY_CLASSES_ROOT\Directory\shell\mbat" /f /v "SubCommands" /t REG_SZ /d "mbat.create;mbat.create-encrypt;mbat.create-verify;mbat.create-encrypt-verify" > nul 2>&1
echo Done

REM Show success message
echo.
echo ***************************************************************************
echo *                                                                         *
echo *                  MBAT context menu created successfully                 *
echo *                                                                         *
echo ***************************************************************************
echo.
pause
echo.
