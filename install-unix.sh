#!/bin/bash

#                             MBAT
#               Copyright (C) 2021  Matt Hudson
#                   Email: matt@mjrhudson.uk

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

###########################################################################
#           TITLE: install-unix.sh                                        #
#         AUTHORS: Matt Hudson (matt@mjrhudson.uk)                        #
# INITIAL RELEASE: 30-November-2020                                       #
#   LAST MODIFIED: 08-February-2021                                       #
###########################################################################

# Cleanup
echo ""
echo "********************* MBAT Unix Install Script v0.1.3 *********************"
echo ""
echo -n "Cleaning up old versions of MBAT if present..."
if [ -L "$HOME/.local/bin/mbat" ]; then
    unlink "$HOME/.local/bin/mbat"
fi
if [ -d "$HOME/.local/scripts/mbat/libs" ]; then
    rm -rf "$HOME/.local/scripts/mbat/libs"
fi
if [ -d "$HOME/.local/scripts/mbat/lib" ]; then
    rm -rf "$HOME/.local/scripts/mbat/lib"
fi
if [ -d "$HOME/.local/scripts/mbat/logs" ]; then
    rm -rf "$HOME/.local/scripts/mbat/logs"
fi
if [ -d "$HOME/.local/scripts/mbat/shell" ]; then
    rm -rf "$HOME/.local/scripts/mbat/shell"
fi
if [ -f "$HOME/.local/scripts/mbat/etc/settings-unix.json" ]; then
    rm -rf "$HOME/.local/scripts/mbat/etc/settings-unix.json"
fi
if [ -f "$HOME/.local/scripts/mbat/LICENCE" ]; then
    rm -rf "$HOME/.local/scripts/mbat/LICENCE"
fi
if [ -f "$HOME/.local/scripts/mbat/README.md" ]; then
    rm -rf "$HOME/.local/scripts/mbat/README.md"
fi
if [ -f "$HOME/.local/scripts/mbat/mbatcli.py" ]; then
    rm -rf "$HOME/.local/scripts/mbat/mbatcli.py"
fi
if [ -f "$HOME/.local/scripts/mbat/runmbat.py" ]; then
    rm -rf "$HOME/.local/scripts/mbat/runmbat.py"
fi
echo "Done"
# Make directories
echo -n "Creating MBAT directories..."
if [ ! -d "$HOME/.local/scripts/mbat/etc" ]; then
    mkdir -p "$HOME/.local/scripts/mbat/etc"
fi
if [ ! -d "$HOME/.local/scripts/mbat/lib" ]; then
    mkdir -p "$HOME/.local/scripts/mbat/lib"
fi
if [ ! -d "$HOME/.local/scripts/mbat/logs" ]; then
    mkdir -p "$HOME/.local/scripts/mbat/logs"
fi
if [ ! -d "$HOME/.local/scripts/mbat/shell" ]; then
    mkdir -p "$HOME/.local/scripts/mbat/shell"
fi
echo "Done"
# Copy files/folders
echo -n "Copying required files/folders..."
ROOTPATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
cp -r "$ROOTPATH/lib" "$HOME/.local/scripts/mbat"
cp -r "$ROOTPATH/shell" "$HOME/.local/scripts/mbat"
cp "$ROOTPATH/LICENCE" "$HOME/.local/scripts/mbat"
cp "$ROOTPATH/README.md" "$HOME/.local/scripts/mbat"
cp "$ROOTPATH/mbatcli.py" "$HOME/.local/scripts/mbat"
echo "Done"
# Make shell script executable
chmod +x "$HOME/.local/scripts/mbat/shell/mbat.sh"
# Create symbolic link
echo -n "Creating symbolic link in $HOME/.local/bin (make sure this is on your PATH)..."
ln -s "$HOME/.local/scripts/mbat/shell/mbat.sh" "$HOME/.local/bin/mbat"
echo "Done"
# Show success message
echo ""
echo "***************************************************************************"
echo "*                                                                         *"
echo "*                       MBAT installed successfully                       *"
echo "*                                                                         *"
echo "*   Note: If MBAT was already installed, settings have been reverted to   *"
echo "*                    default (profiles have been kept)                    *"
echo "*                                                                         *"
echo "***************************************************************************"
echo ""
