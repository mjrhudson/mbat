# MBAT - Matt's Backup and Archiving Tool

Command line based backup/archiving tool written in Python.

## Features

- Cross-platform (tested on Windows, Mac & Linux)
- Lightweight
- Support for GnuPG or OpenSSL encryption of files
- Support for full archive integrity verification during archive creation and extraction
- Support for standalone verification of archive integrity at any point during creation/extraction/long term storage (can split creation/extraction duties to one machine, verify integrity with another to split workload)
- Support for multiple archives at once, either through command line arguments or separate profile files for easier scheduling and automation
- No proprietary formats - MBAT creates .mbtar files that are really just .tar files, and so can be extracted/decrypted on any system, without MBAT (albeit with a bit more effort), so that you don't have to rely on this software being maintained if you need to access your archives 20 years in the future
- Free as in beer and free as in speech
- Uses the GPLv3 licence

## Roadmap

In all honesty, this is a pet project that I work on in my spare time. I only ever really add features as/when I see the need for and have time to implement them. Having said this, I would at least like to get these things done at some point in the near/far/maybe future:

- ~~Support for unverified extractions (where performance is an absolute focus)~~  - **Done!**
- ~~Support for displaying archive information through MBAT (utilising manifest entries)~~  - **Done!**
- ~~Improve archive compression functionality~~  - **Done!**
- ~~Custom output paths for profiles~~  - **Done!**
- ~~Implement custom names for archives~~  - **Done!**
- Proper documentation / commenting - **In Progress**
- General code review with cleanup / optimisations - **In Progress**
- Improve profile functionality (profile archive grouping, subprofiles and more) - **In Progress**
- Support for partial extractions
- Qt-based GUI
- Support for 'delta' backups/archives

## Requirements

The following must be installed and on your system PATH:

- Python v3.6+
- Either:
    - GnuPG v2.2+ (Used as standard for MBAT unless otherwise specified)
    - OpenSSL v1.1.1+

## Installation - Windows

### Automated

1. Use the included `install-win.bat` script to install MBAT automatically
2. (Optional) Use the included `install-win-contextmenu.bat` to add MBAT as a context menu entry (right click menu entry for files/folders in Windows Explorer)

    **NOTE:** This requires administrator privileges

### Manual

1. Copy the MBAT root directory to a location of your choice (%USERPROFILE%\\.local\\scripts is a good place)
2. Copy the `mbat.bat` script found within mbat\\shell\\ to somewhere on the system PATH
3. Ensure that the *mbatpath* variable within `mbat.bat` points to your installation location of MBAT

## Installation - MacOS/Linux

### Automated

1. Use the included `install-unix.sh` script to install MBAT automatically

### Manual

1. Copy the MBAT the root directory to a location of your choice (~/.local/scripts is a good place)
2. Copy the `mbat.sh` script found within mbat/shell/ to somewhere on the system PATH
3. Ensure that the *mbatpath* variable within `mbat.sh` points to your installation location of MBAT

## Quick Start Guide

MBAT is called using the following syntax:

`mbat <mode_specifier> [inputs] ... [options]`

**Note**: Where there is a short version of an *option* available, this can be combined with the initial *mode specifier*, e.g `-c` and `-e` can be combined to make `-ce`. It is **not** possible to combine *mode specifiers* with each other, with the exception of `-v` with either `-c` or `-x`.

The following mode specifiers are available:

| Mode Specifier | Short Version | Explanation |
|----------------|---------------|-------------|
| --create | -c | Create archive |
| --extract | -x | Extract archive |
| --verify | -v | Verify archive |
| --info | -i | Show archive information |
| --help | -h | Show help information |

The following options are available:

| Option | Short Version | Additional Parameters | Explanation |
|--------|---------------|-----------------------|-------------|
| --in |  | */path/to/dir* */path/to/dir2* ... | Input path(s) or profile name |
| --out |  | */path/to/outputdir* | Output path |
| --masterpass |  | [optional] *password* | Use the same password for all archives (if no *password* is supplied after the *--masterpass* command, you will be asked for it at the start of the MBAT run) |
| --encrypt | -e | [optional] *encryption-library* | Encrypt archive using *encryption-library* (if no *encryption-library* is supplied after the *--encrypt* command, GnuPG will be used)
| --profile | -p |  | Enable profile mode |
| --compress | -z |  | Compress archive using gzip |

**NOTE:** Encryption and compression should not be used together. This is due to the fact that the encryption process compresses each file anyway, and so adding on gzip compression provides no benefit and only results in extra file headers.

### Examples

**NOTE:** Short versions of *options*/*mode specifiers* can be in any order when combined, e.g. `-cve` and `-evc` are treated as identical.

*Command:* `mbat -c ~/Documents`

*Explanation:* **C**reate an archive of ~/Documents (output location will be the default, as defined by mbat/etc/settings-win.json or mbat/etc/settings-unix.json, since one was not provided).

*Command:* `mbat -c ~/Documents --out ~/Backups`

*Explanation:* **C**reate an archive of ~/Documents, and create the archive file in ~/Backups.

*Command:* `mbat -c --out ~/Backups --in ~/Documents`

*Explanation:* Same as previous command, but demonstrating how the `--in` flag can be used.

*Command:* `mbat -cvz ~/Documents`

*Explanation:* **C**reate and **v**erify a g**z**ip compressed archive of ~/Documents.

*Command:* `mbat -cve ~/Documents`

*Explanation:* **C**reate and **v**erify an **e**ncrypted archive of ~/Documents.

*Command:* `mbat -pcve example`

*Explanation:* **C**reate and **v**erify **e**ncrypted archives of directories according to the 'example' **p**rofile (mbat/etc/mbatprofile-example.json).

*Command:* `mbat -v ~/Backups/example-mbat-backup.mbtar`

*Explanation:* **V**erify the integrity of the 'example-mbat-backup.mbtar' archive (using the manifest contained within the MBAT archive).

*Command:* `mbat -x --in ~/Backups/example-mbat-backup.mbtar --out ~/Extracted-Backups`

*Explanation:* E**x**tract the 'example-mbat-backup.mbtar' MBAT archive into the ~/Extracted-Backups directory.

*Command:* `mbat -xv --in ~/Backups/example-mbat-backup.mbtar --out ~/Extracted-Backups`

*Explanation:* E**x**tract the 'example-mbat-backup.mbtar' MBAT archive into the ~/Extracted-Backups directory and **v**erify the extraction.

*Command:* `mbat -x --in ~/Backups/example-backups --out ~/Extracted-Backups`

*Explanation:* E**x**tract all MBAT archives contained within the 'example-backups' directory into the ~/Extracted-Backups directory (Note: you will be asked for a password for each individual archive; use the *--masterpass* option to avoid this).

*Command:* `mbat -ce --in example --encrypt openssl --profile --masterpass`

*Explanation:* **C**reate **e**ncrypted archives of directories according to the 'example' **p**rofile, using OpenSSL for encryption and using same password for all archives.

*Command:* `mbat -ce --in example --encrypt openssl --profile --masterpass mysecurepassword`

*Explanation:* Same as previous command, however this time explicity pass the master password to MBAT (so that MBAT does not prompt for a password input during the first run; this is useful for example when executing MBAT using an automated script on a headless machine).

*Command:* `mbat -i ~/Backups/example-mbat-backup.mbtar`

*Explanation:* Show **i**nformation for MBAT archive 'example-mbat-backup.mbtar'.

### Profiles

See either `profiles-win.json` or `profiles-unix.json` in the mbat/etc subdirectory for an example of a profile

## FAQs

...or rather, frequently "anticipated" questions.

- Why?

    I don't want to lose my data

- Couldn't you just use `tar -cvf ...`?

    Yes

- So...?

    I **really** don't want to lose my data, and want integrity checking plus encryption.

- and you couldn't just use `gpg` and `sha1sum`?

    Yes I could, but I make backups a lot, because **I really don't want to lose my data**, so this is quicker overall (or so I keep kidding myself...)

- ...you really don't want to lose your data, right?

    :)

- Ok, more serious, why Python?

    For ease of implementing new features and ease of maintenance. I wrote this to help me, not hinder me (The first iterations of my backup utilities were written in pure batch script, what a pain that was)

- Why use the tarfile module from the Python Standard Library instead of a direct system call to `tar`, when it's included with the majority of modern Windows and Unix systems?

    Cross platform compatibility is key here. I work across both Windows and Linux systems, and I want my archives of my work to be identical (at least in the way they work, not necessarily the data or I'd be doing nothing clearly) no matter which system I'm using. I've encountered discrepencies and issues in the past between `gnutar` (standard for Linux) and `bsdtar` (standard for Windows), so I opted for a slight slowdown in favour of this.

- Why are you not using this convention / doing this the wrong way etc. etc.

    I'm not a trained software engineer, and I taught myself to code from scratch. I probably do some things inefficiently, and probably don't use the correct conventions in certain areas. Feel free to correct me, I'm always open to learning (part of why this is FOSS).

## Issues

Please submit any issues to the issues tracker, I will do my best to keep on top of them where possible.
