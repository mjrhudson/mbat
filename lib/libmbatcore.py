#                             MBAT
#               Copyright (C) 2021  Matt Hudson
#                   Email: matt@mjrhudson.uk

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

###########################################################################
#           TITLE: libmbatcore.py                                         #
#         AUTHORS: Matt Hudson (matt@mjrhudson.uk)                        #
# INITIAL RELEASE: 23-November-2020                                       #
#   LAST MODIFIED: 08-February-2021                                       #
###########################################################################

# SECTION Import Libraries
import os
import platform
import time
import shutil
import subprocess
import json
import tarfile
import zlib
import gzip
import hashlib
import pathlib
import fnmatch
import re
import datetime
import logging
# !SECTION Import Libraries

# SECTION Functions
def getVersionInfo():
    return "0.22.0"

def crc32file(fullpath,cv):
    # cv should be 0 for a new hash
    # (Intended) Only checks data, not filenames
    if os.path.isfile(fullpath):
        with open(fullpath,"rb") as f:
            while True:
                chunk = f.read(65536)
                if not chunk:
                    break
                cv = zlib.crc32(chunk,cv)
        return "%08X" % (cv & 0xFFFFFFFF)

def crc32list(listin,cv):
    # cv should be 0 for a new hash
    for i in range(len(listin)):
        cv = zlib.crc32(listin[i].encode("utf-8"),cv)
    return "%08X" % (cv & 0xFFFFFFFF)

def getMbatFilepaths(targetpaths):
    mbatFilepaths = []
    symlinksignored = 0
    for targetpath in targetpaths:
        if os.path.isfile(targetpath):
            mbatFilepaths = mbatFilepaths + [targetpath]
        elif os.path.isdir(targetpath):
            numFiles = 0
            for pdirs, cdirs, files in os.walk(targetpath):
                for file in files:
                    if fnmatch.fnmatch(file,"*.mbtar") or ("mbat" in file.lower() and (fnmatch.fnmatch(file,"*.tar") or fnmatch.fnmatch(file,"*.tar.gz"))):
                        numFiles += 1
            targetMbatFilepaths = [""]*numFiles
            i = 0
            for pdirs, cdirs, files in os.walk(targetpath):
                for file in files:
                    if fnmatch.fnmatch(file,"*.mbtar") or ("mbat" in file.lower() and (fnmatch.fnmatch(file,"*.tar") or fnmatch.fnmatch(file,"*.tar.gz"))):
                        targetMbatFilepaths[i] = os.path.join(pdirs,file)
                        i += 1
            # Delete broken symbolic links
            for i in range(len(targetMbatFilepaths)):
                idx = len(targetMbatFilepaths)-1-i
                if not os.path.exists(targetMbatFilepaths[idx]):
                    symlinksignored += 1
                    del targetMbatFilepaths[idx]
            mbatFilepaths = mbatFilepaths + targetMbatFilepaths
        else:
            logging.error("\"%s\" does not lead to a file or folder",targetpath)
            print("Error: \"" + targetpath + "\" does not lead to a file or folder.\n")
            exit()
    for i in range(len(mbatFilepaths)):
        if not os.path.isfile(mbatFilepaths[i]):
            logging.error("\"%s\" does not lead to a valid file",targetpath)
            print("Error: \"" + targetpath + "\" does not lead to a valid file.\n")
            exit()
    return mbatFilepaths, symlinksignored

def getArcList(targetpath):
    idx = 0
    for pdirs, cdirs, files in os.walk(targetpath):
        for cdir in cdirs:
            idx += 1
        for file in files:
            idx += 1
    pathlist = [""]*idx
    idx = 0
    for pdirs, cdirs, files in os.walk(targetpath):
        for cdir in cdirs:
            relcdir = os.path.relpath(os.path.join(pdirs,cdir),targetpath)
            pathlist[idx] = str(relcdir)
            idx += 1
        for file in files:
            relfile = os.path.relpath(os.path.join(pdirs,file),targetpath)
            pathlist[idx] = str(relfile)
            idx += 1
    # Delete broken symbolic links
    symlinksignored = 0
    for i in range(len(pathlist)):
        idx = len(pathlist)-1-i
        subpath = targetpath + os.path.sep + pathlist[idx]
        if not os.path.exists(subpath):
            symlinksignored += 1
            del pathlist[idx]
    idx = 0
    for i in range(len(pathlist)):
        subpath = targetpath + os.path.sep + pathlist[i]
        if os.path.isfile(subpath):
            idx += 1
        elif os.path.isdir(subpath):
            if len(os.listdir(subpath)) == 0:
                idx += 1
    arclist = [""]*idx
    idx = 0
    emptydirs = 0
    for j in range(len(pathlist)):
        subpath = targetpath + os.path.sep + pathlist[j]
        if os.path.isfile(subpath):
            arclist[idx] = pathlist[j]
            idx += 1
        elif os.path.isdir(subpath):
            if len(os.listdir(subpath)) == 0:
                arclist[idx] = pathlist[j]
                idx += 1
                emptydirs += 1
    logging.debug("Archive list debug information")
    logging.debug("Number of empty directories to be included: %d",emptydirs)
    logging.debug("Number of symbolic links to be ignored: %d", symlinksignored)
    return arclist, emptydirs, symlinksignored

def escapeSpecialChars(strin):
    specialChars = {
        "dollar": ["$","$","\\$"]
    }
    strout = strin
    for key in specialChars:
        if platform.system() == "Windows":
            strout = strout.replace(specialChars[key][0],specialChars[key][1])
        else:
            strout = strout.replace(specialChars[key][0],specialChars[key][2])
    return strout

def logListStatus(listNum,listTotal,listOperation,listName,listPaddingNum,printType,printVerificationLog,fileID):
    logging.info("(%d of %d) %s %s",listNum,listTotal,listOperation,listName)
    if printVerificationLog:
        fileID.write(datetime.datetime.today().strftime('[%Y-%m-%d %H:%M:%S] ') + "(" + str(listNum) + " of " + str(listTotal) + ") " + listOperation + " " + listName + "\n")
    if listPaddingNum > len(listOperation):
        listPadding = " "*(listPaddingNum-len(listOperation))
    else:
        listPadding = " "
    if printType == 1:
        print("(%d of %d) %s %s%s" % (listNum,listTotal,listOperation,listName,listPadding),end="",flush=True)
    elif printType == 2:
        print("\r(%d of %d) %s %s%s" % (listNum,listTotal,listOperation,listName,listPadding))

def getArchiveInfo(archivesettings):
    status = 2
    if os.path.isfile(archivesettings["archivefilepath"]) == False:
        logging.error("The entered path does not lead to a file")
        print("Error: The entered path does not lead to a file.\n")
        exit()
    
    temppath = str(pathlib.Path(__file__).parent.parent.absolute()) + os.path.sep + ".temp"
    if os.path.isdir(temppath):
        shutil.rmtree(temppath)
    os.mkdir(temppath)

    with tarfile.open(archivesettings["archivefilepath"],format=tarfile.GNU_FORMAT) as tar:
        tar.extract("manifest.json",path=temppath)

    tempmanifestpath = str(temppath + os.path.sep + "manifest.json")

    with open(tempmanifestpath,"r") as mf:
        manifest = json.load(mf)

    os.remove(tempmanifestpath)
    if os.path.isdir(temppath):
        shutil.rmtree(temppath)
    if status == 2:
        status = 1
    return status, manifest

def createArchive(archivesettings):
    status = 2
    if os.path.isdir(archivesettings["targetpath"]) == False:
        logging.error("The entered path does not lead to a folder")
        print("Error: The entered path does not lead to a folder.\n")
        exit()
    
    arctimestamp = datetime.datetime.today()
    archivedate = arctimestamp.strftime("%Y") + arctimestamp.strftime("%m") + arctimestamp.strftime("%d")
    archivetime = arctimestamp.strftime("%H") + arctimestamp.strftime("%M") + arctimestamp.strftime("%S")

    inputsplit = os.path.split(archivesettings["targetpath"])
    inputname = inputsplit[1].replace(" ","-")

    archivesettings["archivename"] = archivesettings["archivename"]\
        .replace("%Y",arctimestamp.strftime("%Y"))\
        .replace("%m",arctimestamp.strftime("%m"))\
        .replace("%d",arctimestamp.strftime("%d"))\
        .replace("%H",arctimestamp.strftime("%H"))\
        .replace("%M",arctimestamp.strftime("%M"))\
        .replace("%S",arctimestamp.strftime("%S"))\
        .replace("$TARGETNAME",inputname)
    
    temppath = str(pathlib.Path(__file__).parent.parent.absolute()) + os.path.sep + ".temp"
    if os.path.isdir(temppath):
        shutil.rmtree(temppath)
    os.mkdir(temppath)

    tarextension = ".mbtar"
    archivefilepath = archivesettings["outputpath"] + os.path.sep + archivesettings["archivename"] + tarextension

    manifestpath = temppath + os.path.sep + "manifest.json"

    if "e" in archivesettings["modeFlag"] and archivesettings["password"] == "":
        logging.error("No password provided for encrypted archive")
        print("Error: No password provided for encrypted archive.\n")
        exit()
    
    tArcStart = time.perf_counter()
    logging.info("Creating archive: %s",archivefilepath)
    print("Creating archive: " + archivefilepath + "\n")

    arclist, emptydirs, symlinksignored = getArcList(archivesettings["targetpath"])
    if emptydirs != 0:
        if emptydirs == 1:
            logging.info("%d empty directory to be included",emptydirs)
            print(str(emptydirs) + " empty directory to be included\n")
        else:
            logging.info("%d empty directories to be included",emptydirs)
            print(str(emptydirs) + " empty directories to be included\n")
    if symlinksignored != 0:
        if symlinksignored == 1:
            logging.info("%d symbolic link to be ignored",symlinksignored)
            print(str(symlinksignored) + " symbolic link to be ignored\n")
        else:
            logging.info("%d symbolic links to be ignored",symlinksignored)
            print(str(symlinksignored) + " symbolic links to be ignored\n")

    manifestInfo = ["","","","","","","",""] # MBAT version / date / time / mode used (c for plain creation, ce for created with encryption etc.) / num files / num empty dirs / time to complete (HH:MM:SS) / extra information
    systemInfo = ["",""] # Hostname / Operating system
    manifestTree = ["","",""] # Input path / tree names hash / tree data hash
    manifestFiles = {}
    manifestEmptyDirs = {}

    manifestInfo[0] = archivesettings["mbatversion"]
    manifestInfo[1] = archivedate
    manifestInfo[2] = archivetime
    manifestInfo[3] = archivesettings["modeFlag"]

    systemInfo[0] = platform.node()
    systemInfo[1] = platform.platform()

    manifestTree[0] = archivesettings["targetpath"].replace("\\","/")
    
    manifestInfo[7] = "Python version: " + platform.python_version()
    if "e" in archivesettings["modeFlag"]:
        manifestInfo[7] = manifestInfo[7] + "; "
        if archivesettings["libEnc"] == "gpg":
            manifestInfo[7] = manifestInfo[7] + "Encryption library: " + archivesettings["libEncVersion"]
        elif archivesettings["libEnc"] == "openssl":
            manifestInfo[7] = manifestInfo[7] + "Encryption library: " + archivesettings["libEncVersion"]
    
    logging.info("Writing to archive: %s",archivefilepath)
    print("Writing to archive: " + archivefilepath + "\n")

    arcfilecount = 0
    arcemptydircount = 0
    tarwritemode = "w"
    with tarfile.open(archivefilepath,tarwritemode,format=tarfile.GNU_FORMAT) as tar:
        for i in range(len(arclist)):
            subpath = archivesettings["targetpath"] + os.path.sep + arclist[i] # Full filepath to file
            logListStatus(i+1,len(arclist),"ADDING",str(os.path.split(archivesettings["targetpath"])[1] + os.path.sep + arclist[i]).replace("\\","/"),16,1,False,0)
            encpath = ""
            try:
                if os.path.isfile(subpath):
                    # checkValue *Pre and *Post denote the checksums before and after applying encryption and/or compression
                    checkValuePre = crc32file(subpath,0)
                    checkValuePost = checkValuePre # *Post will be the same as *Pre if neither encryption or compression is used
                    arclistname = os.path.split(archivesettings["targetpath"])[1] + os.path.sep + arclist[i] # Filepath to store file under in archive
                    arclistpath = subpath
                    if "e" in archivesettings["modeFlag"]:
                        arclistname = arclistname + ".enc"
                        encpath = temppath + os.path.sep + os.path.split(arclistpath)[1] + ".enc" # os.path.split(arclistpath)[1] gets just the filename from the filepath
                        if archivesettings["libEnc"] == "gpg":
                            enccommand = str("gpg --symmetric --s2k-cipher-algo AES256 --s2k-digest-algo SHA512 --s2k-mode 3 --s2k-count 10000 --output \"" + escapeSpecialChars(str(encpath)) + "\" --batch --yes --pinentry-mode=loopback --passphrase=\"" + archivesettings["password"] + "\" --no-tty --no-verbose --quiet \"" + escapeSpecialChars(str(arclistpath)) + "\"")
                        elif archivesettings["libEnc"] == "openssl":
                            enccommand = str("openssl aes-256-cbc -md sha512 -iter 10000 -pbkdf2 -salt -k \"" + archivesettings["password"] + "\" -in \"" + escapeSpecialChars(str(arclistpath)) + "\" -out \"" + escapeSpecialChars(str(encpath)) + "\"")
                        try:
                            subprocess.run(enccommand,shell=True)
                        except:
                            logListStatus(i+1,len(arclist),"FAILED",str(os.path.split(archivesettings["targetpath"])[1] + os.path.sep + arclist[i]).replace("\\","/"),16,2,False,0)
                            logging.error("Encryption failed for \"%s\"",arclist[i].replace("\\","/"))
                            print("\nError: Encryption failed for \"" + arclist[i].replace("\\","/") + "\".\n")
                            exit()
                        if not os.path.isfile(encpath):
                            logListStatus(i+1,len(arclist),"FAILED",str(os.path.split(archivesettings["targetpath"])[1] + os.path.sep + arclist[i]).replace("\\","/"),16,2,False,0)
                            logging.error("Encryption failed for \"%s\"",arclist[i].replace("\\","/"))
                            print("\nError: Encryption failed for \"" + arclist[i].replace("\\","/") + "\".\n")
                            exit()
                        arclistpath = encpath
                        checkValuePost = crc32file(encpath,0)
                    if "z" in archivesettings["modeFlag"]:
                        arclistname = arclistname + ".gz"
                        cmppath = temppath + os.path.sep + os.path.split(arclistpath)[1] + ".gz"
                        try:
                            with open(arclistpath,"rb") as cmpin:
                                with gzip.open(cmppath,"wb") as cmpout:
                                    shutil.copyfileobj(cmpin,cmpout)
                        except:
                            logListStatus(i+1,len(arclist),"FAILED",str(os.path.split(archivesettings["targetpath"])[1] + os.path.sep + arclist[i]).replace("\\","/"),16,2,False,0)
                            logging.error("Compression failed for \"%s\"",arclist[i].replace("\\","/"))
                            print("\nError: Compression failed for \"" + arclist[i].replace("\\","/") + "\".\n")
                            exit()
                        if not os.path.isfile(cmppath):
                            logListStatus(i+1,len(arclist),"FAILED",str(os.path.split(archivesettings["targetpath"])[1] + os.path.sep + arclist[i]).replace("\\","/"),16,2,False,0)
                            logging.error("Compression failed for \"%s\"",arclist[i].replace("\\","/"))
                            print("\nError: Compression failed for \"" + arclist[i].replace("\\","/") + "\".\n")
                            exit()
                        arclistpath = cmppath
                        checkValuePost = crc32file(cmppath,0)
                    hashValue = hashlib.sha1(arclistname.replace("\\","/").encode("utf-8")).hexdigest()
                    tar.add(arclistpath,arcname=arclistname.replace("\\","/"))
                    arcfilecount += 1
                    manifestFiles[hashValue] = [arclistname.replace("\\","/"),checkValuePre,checkValuePost]
                    if "e" in archivesettings["modeFlag"]:
                        os.remove(str(encpath))
                    if "z" in archivesettings["modeFlag"]:
                        os.remove(str(cmppath))
                elif os.path.isdir(subpath):
                    arclistname = os.path.split(archivesettings["targetpath"])[1] + os.path.sep + arclist[i]
                    tar.add(subpath,arcname=arclistname.replace("\\","/"))
                    if len(arclistname) >= 100:
                        arclistname = arclistname + os.path.sep
                    hashValue = hashlib.sha1(arclistname.replace("\\","/").encode("utf-8")).hexdigest()
                    arcemptydircount += 1
                    manifestEmptyDirs[hashValue] = arclistname.replace("\\","/")
            except:
                if os.path.isfile(encpath):
                    os.remove(str(encpath))
                logListStatus(i+1,len(arclist),"FAILED",str(os.path.split(archivesettings["targetpath"])[1] + os.path.sep + arclist[i]).replace("\\","/"),16,2,False,0)
                logging.error("Could not add \"%s\" to archive",arclist[i].replace("\\","/"))
                print("\nError: Could not add \""+ arclist[i].replace("\\","/") + "\" to archive.\n")
                exit()
            logListStatus(i+1,len(arclist),"ADDED",str(os.path.split(archivesettings["targetpath"])[1] + os.path.sep + arclist[i]).replace("\\","/"),16,2,False,0)
        manifestInfo[4] = str(arcfilecount)
        manifestInfo[5] = str(arcemptydircount)

        sortedTreeNamesHashList = sorted(list(manifestFiles.keys()) + list(manifestEmptyDirs.keys()))
        treeNamesHash = crc32list(sortedTreeNamesHashList,0)
        manifestTree[1] = treeNamesHash
        
        sortedHashList = sorted(list(manifestFiles.keys()))
        dataHashList = [""]*len(sortedHashList)
        for i in range(len(sortedHashList)):
            dataHashList[i] = manifestFiles[sortedHashList[i]][1]
        treeDataHash = crc32list(dataHashList,0)
        manifestTree[2] = treeDataHash
        
        tArcEnd = time.perf_counter()
        manifestInfo[6] = time.strftime("%H:%M:%S",time.gmtime(tArcEnd-tArcStart))
        manifest = {"info": manifestInfo, "system": systemInfo, "tree": manifestTree, "files": manifestFiles, "emptydirs": manifestEmptyDirs}

        with open(manifestpath,"w") as f:
            f.write(json.dumps(manifest,indent=4))
            f.write("\n")
        tar.add(manifestpath,arcname="manifest.json")
        os.remove(str(manifestpath))

    if os.path.isdir(temppath):
        shutil.rmtree(temppath)
    
    logging.info("Write completed")
    print("\nWrite completed\n")
    if status == 2:
        status = 1
    return archivefilepath, status

def extractArchive(archivesettings):
    status = 2
    if os.path.isfile(archivesettings["archivefilepath"]) == False:
        logging.error("The entered path does not lead to a file")
        print("Error: The entered path does not lead to a file.\n")
        exit()
    
    temppath = str(pathlib.Path(__file__).parent.parent.absolute()) + os.path.sep + ".temp"
    if os.path.isdir(temppath):
        shutil.rmtree(temppath)
    os.mkdir(temppath)
    
    with tarfile.open(archivesettings["archivefilepath"],format=tarfile.GNU_FORMAT) as tar:
        tar.extract("manifest.json",path=temppath)

    tempmanifestpath = str(temppath + os.path.sep + "manifest.json")

    with open(tempmanifestpath,"r") as mf:
        manifest = json.load(mf)
    
    if "e" in manifest["info"][3] and archivesettings["password"] == "":
        logging.error("No password provided for encrypted archive")
        print("Error: No password provided for encrypted archive.\n")
        exit()
    
    logging.info("Extracting archive: %s",archivesettings["archivefilepath"])
    print("Extracting archive: " + archivesettings["archivefilepath"] + "\n")

    if os.path.isdir(archivesettings["outputpath"] + os.path.sep + os.path.split(manifest["tree"][0])[1]):
        logging.warning("The folder \"%s\" already exists in \"%s\"",os.path.split(manifest["tree"][0])[1],archivesettings["outputpath"])
        print("Warning: The folder \"" + os.path.split(manifest["tree"][0])[1] + "\" already exists in \"" + archivesettings["outputpath"] + "\".\n")
        logging.debug("Asking user whether to delete folder")
        deleteOutput = input("Do you want to delete this folder (Note: Choosing no will exit MBAT)? (Y/N): ")
        print("")
        if deleteOutput.lower() == "y":
            logging.debug("User chose to delete folder")
            shutil.rmtree(archivesettings["outputpath"] + os.path.sep + os.path.split(manifest["tree"][0])[1])
        else:
            logging.debug("User did not choose to delete folder")
            exit()

    with tarfile.open(archivesettings["archivefilepath"],format=tarfile.GNU_FORMAT) as tar:
        arclist = tar.getnames()
    if "manifest.json" in arclist:
        arclist.remove("manifest.json")
    if "verification.txt" in arclist:
        arclist.remove("verification.txt")
    
    manifestFileHashes = manifest["files"].keys()
    manifestEmptyDirHashes = manifest["emptydirs"].keys()
    arcfilecount = int(manifest["info"][4])
    extractedfilecount = 0
    idx = 0
    hashfilepathlist = [""]*arcfilecount
    exfilepathlist = [""]*arcfilecount
    exrelpathlist = [""]*arcfilecount
    for arcfilepath in arclist:
        hashfilepath = hashlib.sha1(arcfilepath.replace("\\","/").encode("utf-8")).hexdigest()
        if hashfilepath in manifestFileHashes:
            arcfiletype = "file"
        elif hashfilepath in manifestEmptyDirHashes:
            arcfiletype = "dir"
        else:
            logging.error("\"%s\" is missing from the archive manifest",arcfilepath.replace("\\","/"))
            print("\nError: \""+ arcfilepath.replace("\\","/") + "\" is missing from the archive manifest.\n")
            exit()
        with tarfile.open(archivesettings["archivefilepath"],format=tarfile.GNU_FORMAT) as tar:
            tar.extract(arcfilepath.replace("\\","/"),path=archivesettings["outputpath"])
        exrelpath = arcfilepath
        exfilepath = archivesettings["outputpath"] + os.path.sep + exrelpath
        if arcfiletype == "file":
            logListStatus(idx+1,arcfilecount,"EXTRACTING",exrelpath.replace("\\","/"),16,1,False,0)
            if not os.path.isfile(exfilepath):
                logListStatus(idx+1,arcfilecount,"FAILED",exrelpath.replace("\\","/"),16,2,False,0)
                logging.error("Extraction failed for \"%s\"",exrelpath.replace("\\","/"))
                print("\nError: Extraction failed for \""+ exrelpath.replace("\\","/") + "\".\n")
                exit()
            if "z" in manifest["info"][3]:
                exfilepathcmp = exfilepath
                exfilepath = re.sub(r"\.gz$","",str(exfilepathcmp))
                exrelpath = re.sub(r"\.gz$","",str(exrelpath))
                try:
                    with gzip.open(exfilepathcmp,"rb") as cmpin:
                        with open(exfilepath,"wb") as cmpout:
                            shutil.copyfileobj(cmpin,cmpout)
                except:
                    logListStatus(idx+1,arcfilecount,"FAILED",exrelpath.replace("\\","/"),16,2,False,0)
                    logging.error("Decompression failed for \"%s\"",exrelpath.replace("\\","/"))
                    print("\nError: Decompression failed for \""+ exrelpath.replace("\\","/") + "\".\n")
                    if os.path.isfile(exfilepathcmp):
                        os.remove(str(exfilepathcmp))
                    exit()
                if not os.path.isfile(exfilepath):
                    logListStatus(idx+1,arcfilecount,"FAILED",exrelpath.replace("\\","/"),16,2,False,0)
                    logging.error("Decompression failed for \"%s\"",exrelpath.replace("\\","/"))
                    print("\nError: Decompression failed for \""+ exrelpath.replace("\\","/") + "\".\n")
                    if os.path.isfile(exfilepathcmp):
                        os.remove(str(exfilepathcmp))
                    exit()
            if "e" in manifest["info"][3]:
                exfilepathenc = exfilepath
                exfilepath = re.sub(r"\.enc$","",str(exfilepathenc))
                exrelpath = re.sub(r"\.enc$","",str(exrelpath))
                if "gpg" in manifest["info"][-1].lower():
                    deccommand = str("gpg --batch --yes --pinentry-mode=loopback --passphrase=\"" + archivesettings["password"] + "\" --output \"" + escapeSpecialChars(str(exfilepath)) + "\" --no-tty --no-verbose --quiet --decrypt \"" + escapeSpecialChars(str(exfilepathenc)) + "\"")
                elif "openssl" in manifest["info"][-1].lower():
                    deccommand = str("openssl aes-256-cbc -md sha512 -iter 10000 -pbkdf2 -d -k \"" + archivesettings["password"] + "\" -in \"" + escapeSpecialChars(str(exfilepathenc)) + "\" -out \"" + escapeSpecialChars(str(exfilepath)) + "\"")
                try:
                    subprocess.run(deccommand,shell=True)
                except:
                    logListStatus(idx+1,arcfilecount,"FAILED",exrelpath.replace("\\","/"),16,2,False,0)
                    logging.error("Decryption failed for \"%s\"",exrelpath.replace("\\","/"))
                    print("\nError: Decryption failed for \""+ exrelpath.replace("\\","/") + "\".\n")
                    if os.path.isfile(exfilepathenc):
                        os.remove(str(exfilepathenc))
                    exit()
                if not os.path.isfile(exfilepath):
                    logListStatus(idx+1,arcfilecount,"FAILED",exrelpath.replace("\\","/"),16,2,False,0)
                    logging.error("Decryption failed for \"%s\"",exrelpath.replace("\\","/"))
                    print("\nError: Decryption failed for \""+ exrelpath.replace("\\","/") + "\".\n")
                    if os.path.isfile(exfilepathenc):
                        os.remove(str(exfilepathenc))
                    exit()
            if "z" in manifest["info"][3]:
                os.remove(str(exfilepathcmp))
            if "e" in manifest["info"][3]:
                os.remove(str(exfilepathenc))
            hashfilepathlist[idx] = hashfilepath
            exfilepathlist[idx] = exfilepath
            exrelpathlist[idx] = exrelpath
            logListStatus(idx+1,arcfilecount,"EXTRACTED",exrelpath.replace("\\","/"),16,2,False,0)
            idx += 1
            extractedfilecount += 1
        elif arcfiletype == "dir":
            if not os.path.isdir(exfilepath):
                logging.error("Extraction failed for \"%s\"",exrelpath.replace("\\","/"))
                print("\nError: Extraction failed for \""+ exrelpath.replace("\\","/") + "\".\n")
                exit()
    
    if "v" in archivesettings["modeFlag"]:
        print("\nVerifying archive: " + archivesettings["archivefilepath"] + "\n")
        failedFiles = ""
        verifiedfilecount = 0
        idx = 0
        for i in range(len(exfilepathlist)):
            hashfilepath = hashfilepathlist[i]
            exfilepath = exfilepathlist[i]
            exrelpath = exrelpathlist[i]
            logListStatus(idx+1,arcfilecount,"VERIFYING",exrelpath.replace("\\","/"),16,1,False,0)
            
            manifestEntry = manifest["files"][hashfilepath]
            exCheckValuePre = crc32file(exfilepath,0)
            if manifestEntry[1] == exCheckValuePre:
                logListStatus(idx+1,arcfilecount,"VERIFIED",exrelpath.replace("\\","/"),16,2,False,0)
                verifiedfilecount += 1
            else:
                logListStatus(idx+1,arcfilecount,"FAILED",exrelpath.replace("\\","/"),16,2,False,0)
                failedFiles = failedFiles + "    " + exrelpath.replace("\\","/") + "\n"
                print("\nWarning: Verification failed for \""+ exrelpath.replace("\\","/") + "\".\n")
            idx +=1
        
        if extractedfilecount == verifiedfilecount:
            archiveFilesVerified = True
        else:
            archiveFilesVerified = False
        
        logging.info("VERIFYING Archive tree")
        print("\nVERIFYING Archive tree",end="",flush=True)

        exoutputpath = archivesettings["outputpath"] + os.path.sep + os.path.split(manifest["tree"][0])[1]
        exarclist = getArcList(exoutputpath)[0]
        
        namesHashList = [""]*len(exarclist)
        for i in range(len(exarclist)):
            exarclistname = os.path.split(manifest["tree"][0])[1] + os.path.sep + exarclist[i]
            if "e" in manifest["info"][3] and os.path.isfile(exoutputpath + os.path.sep + exarclist[i]):
                exarclistname = exarclistname + ".enc"
            if "z" in manifest["info"][3] and os.path.isfile(exoutputpath + os.path.sep + exarclist[i]):
                exarclistname = exarclistname + ".gz"
            namesHashList[i] = hashlib.sha1(exarclistname.replace("\\","/").encode("utf-8")).hexdigest()
        
        sortedTreeNamesHashList = sorted(namesHashList)
        treeNamesHash = crc32list(sortedTreeNamesHashList,0)

        if manifest["tree"][1] == treeNamesHash:
            archiveTreeVerified = True
            logging.info("VERIFIED Archive tree")
            print("\rVERIFIED Archive tree \n")
        else:
            archiveTreeVerified = False
            logging.warning("FAILED Archive tree")
            print("\rFAILED Archive tree   ")
            print("\nWarning: Verification failed for archive tree.\n")
        
        if archiveTreeVerified and archiveFilesVerified:
            logging.info("Extraction successful")
            print("Extraction successful\n")
        else:
            status = 0
            if not archiveTreeVerified and not archiveFilesVerified:
                logging.warning("Extraction succeeded but both the archive tree and the following file(s) failed verification: %s",failedFiles.replace("\n","; "))
                print("Extraction succeeded but both the archive tree and the following file(s) failed verification:\n")
                print(failedFiles)
            if not archiveTreeVerified:
                logging.warning("Extraction succeeded but the archive tree failed verification")
                print("Extraction succeeded but the archive tree failed verification\n")
            if not archiveFilesVerified:
                logging.warning("Extraction succeeded but the following file(s) failed verification: %s",failedFiles.replace("\n","; "))
                print("Extraction succeeded but the following file(s) failed verification:\n")
                print(failedFiles)
    else:
        logging.info("Extraction successful")
        print("\nExtraction successful\n")
    
    os.remove(tempmanifestpath)
    if os.path.isdir(temppath):
        shutil.rmtree(temppath)
    if status == 2:
        status = 1
    return status

def verifyArchive(archivesettings):
    status = 2
    if os.path.isfile(archivesettings["archivefilepath"]) == False:
        logging.error("The entered path does not lead to a file")
        print("Error: The entered path does not lead to a file.\n")
        exit()
    
    temppath = str(pathlib.Path(__file__).parent.parent.absolute()) + os.path.sep + ".temp"
    if os.path.isdir(temppath):
        shutil.rmtree(temppath)
    os.mkdir(temppath)

    with tarfile.open(archivesettings["archivefilepath"],format=tarfile.GNU_FORMAT) as tar:
        tar.extract("manifest.json",path=temppath)

    tempmanifestpath = str(temppath + os.path.sep + "manifest.json")

    with open(tempmanifestpath,"r") as mf:
        manifest = json.load(mf)
    
    if "e" in manifest["info"][3] and archivesettings["password"] == "":
        logging.error("No password provided for encrypted archive")
        print("Error: No password provided for encrypted archive.\n")
        exit()
    
    vlogfilename = "verification.txt"
    vlogfilepath = temppath + os.path.sep + vlogfilename
    vlogfile = open(vlogfilepath,"w")
    
    logging.info("Verifying archive: %s",archivesettings["archivefilepath"])
    vlogfile.write(datetime.datetime.today().strftime('[%Y-%m-%d %H:%M:%S] ') + "Verifying archive: " + archivesettings["archivefilepath"] + "\n\n")
    print("Verifying archive: " + archivesettings["archivefilepath"] + "\n")
    
    logging.info("Verifying files in archive")
    vlogfile.write(datetime.datetime.today().strftime('[%Y-%m-%d %H:%M:%S] ') + "Verifying files in archive\n\n")
    print("Verifying files in archive\n")

    arcfilecount = int(manifest["info"][4])
    verifiedcount = 0
    idx = 0
    failedFiles = ""
    for hashValue in manifest["files"]:
        manifestEntry = manifest["files"][hashValue]
        arcfilepath = manifestEntry[0]
        valrelpath = arcfilepath
        valfilepath = temppath + os.path.sep + valrelpath
        logListStatus(idx+1,arcfilecount,"VERIFYING",valrelpath.replace("\\","/"),16,1,True,vlogfile)
        with tarfile.open(archivesettings["archivefilepath"],format=tarfile.GNU_FORMAT) as tar:
            tar.extract(arcfilepath.replace("\\","/"),path=temppath)
        valCheckValuePost = crc32file(valfilepath,0)
        valCheckValuePre = valCheckValuePost
        if "z" in manifest["info"][3]:
            valfilepathcmp = valfilepath
            valfilepath = re.sub(r"\.gz$","",str(valfilepathcmp))
            valrelpath = re.sub(r"\.gz$","",str(valrelpath))
            try:
                with gzip.open(valfilepathcmp,"rb") as cmpin:
                    with open(valfilepath,"wb") as cmpout:
                        shutil.copyfileobj(cmpin,cmpout)
            except:
                logListStatus(idx+1,arcfilecount,"FAILED",valrelpath.replace("\\","/"),16,2,True,vlogfile)
                logging.error("Decompression failed for \"%s\"",valrelpath.replace("\\","/"))
                vlogfile.write("\n" + datetime.datetime.today().strftime('[%Y-%m-%d %H:%M:%S] ') + "Decompression failed for \""+ valrelpath.replace("\\","/") + "\".\n")
                vlogfile.close()
                print("\nError: Decompression failed for \""+ valrelpath.replace("\\","/") + "\".\n")
                if os.path.isfile(valfilepathcmp):
                    os.remove(str(valfilepathcmp))
                exit()
            if not os.path.isfile(valfilepath):
                logListStatus(idx+1,arcfilecount,"FAILED",valrelpath.replace("\\","/"),16,2,True,vlogfile)
                logging.error("Decompression failed for \"%s\"",valrelpath.replace("\\","/"))
                vlogfile.write("\n" + datetime.datetime.today().strftime('[%Y-%m-%d %H:%M:%S] ') + "Decompression failed for \""+ valrelpath.replace("\\","/") + "\".\n")
                vlogfile.close()
                print("\nError: Decompression failed for \""+ valrelpath.replace("\\","/") + "\".\n")
                if os.path.isfile(valfilepathcmp):
                    os.remove(str(valfilepathcmp))
                exit()
            valCheckValuePre = crc32file(valfilepath,0)
        if "e" in manifest["info"][3]:
            valfilepathenc = valfilepath
            valfilepath = re.sub(r"\.enc$","",str(valfilepathenc))
            valrelpath = re.sub(r"\.enc$","",str(valrelpath))
            if "gpg" in manifest["info"][-1].lower():
                deccommand = str("gpg --batch --yes --pinentry-mode=loopback --passphrase=\"" + archivesettings["password"] + "\" --output \"" + escapeSpecialChars(str(valfilepath)) + "\" --no-tty --no-verbose --quiet --decrypt \"" + escapeSpecialChars(str(valfilepathenc)) + "\"")
            elif "openssl" in manifest["info"][-1].lower():
                deccommand = str("openssl aes-256-cbc -md sha512 -iter 10000 -pbkdf2 -d -k \"" + archivesettings["password"] + "\" -in \"" + escapeSpecialChars(str(valfilepathenc)) + "\" -out \"" + escapeSpecialChars(str(valfilepath)) + "\"")
            try:
                subprocess.run(deccommand,shell=True)
            except:
                logListStatus(idx+1,arcfilecount,"FAILED",valrelpath.replace("\\","/"),16,2,True,vlogfile)
                logging.error("Decryption failed for \"%s\"",valrelpath.replace("\\","/"))
                vlogfile.write("\n" + datetime.datetime.today().strftime('[%Y-%m-%d %H:%M:%S] ') + "Decryption failed for \""+ valrelpath.replace("\\","/") + "\".\n")
                vlogfile.close()
                print("\nError: Decryption failed for \""+ valrelpath.replace("\\","/") + "\".\n")
                if os.path.isfile(valfilepathenc):
                    os.remove(str(valfilepathenc))
                exit()
            if not os.path.isfile(valfilepath):
                logListStatus(idx+1,arcfilecount,"FAILED",valrelpath.replace("\\","/"),16,2,True,vlogfile)
                logging.error("Decryption failed for \"%s\"",valrelpath.replace("\\","/"))
                vlogfile.write("\n" + datetime.datetime.today().strftime('[%Y-%m-%d %H:%M:%S] ') + "Decryption failed for \""+ valrelpath.replace("\\","/") + "\".\n")
                vlogfile.close()
                print("\nError: Decryption failed for \""+ valrelpath.replace("\\","/") + "\".\n")
                if os.path.isfile(valfilepathenc):
                    os.remove(str(valfilepathenc))
                exit()
            valCheckValuePre = crc32file(valfilepath,0)
        if "z" in manifest["info"][3]:
            os.remove(str(valfilepathcmp))
        if "e" in manifest["info"][3]:
            os.remove(str(valfilepathenc))
        os.remove(str(valfilepath))
        if manifestEntry[1] == valCheckValuePre and manifestEntry[2] == valCheckValuePost:
            logListStatus(idx+1,arcfilecount,"VERIFIED",valrelpath.replace("\\","/"),16,2,True,vlogfile)
            verifiedcount += 1
        else:
            logListStatus(idx+1,arcfilecount,"FAILED",valrelpath.replace("\\","/"),16,2,True,vlogfile)
            failedFiles = failedFiles + "    " + valrelpath.replace("\\","/") + "\n"
            vlogfile.write("\n" + datetime.datetime.today().strftime('[%Y-%m-%d %H:%M:%S] ') + "Warning: Verification failed for \""+ valrelpath.replace("\\","/") + "\".\n")
            print("\nWarning: Verification failed for \""+ valrelpath.replace("\\","/") + "\".\n")
        idx += 1

    if idx == verifiedcount:
        archiveFilesVerified = True
    else:
        archiveFilesVerified = False
    
    logging.info("VERIFYING Archive tree")
    vlogfile.write(datetime.datetime.today().strftime('[%Y-%m-%d %H:%M:%S] ') + "VERIFYING Archive tree\n\n")
    print("\nVERIFYING Archive tree",end="",flush=True)

    with tarfile.open(archivesettings["archivefilepath"],format=tarfile.GNU_FORMAT) as tar:
        arclist = tar.getnames()
    if "manifest.json" in arclist:
        arclist.remove("manifest.json")
    if "verification.txt" in arclist:
        arclist.remove("verification.txt")
    
    namesHashList = [""]*len(arclist)
    for i in range(len(arclist)):
        namesHashList[i] = hashlib.sha1(arclist[i].replace("\\","/").encode("utf-8")).hexdigest()
    
    sortedTreeNamesHashList = sorted(namesHashList)
    treeNamesHash = crc32list(sortedTreeNamesHashList,0)
    
    if manifest["tree"][1] == treeNamesHash:
        archiveTreeVerified = True
        logging.info("VERIFIED Archive tree")
        vlogfile.write(datetime.datetime.today().strftime('[%Y-%m-%d %H:%M:%S] ') + "VERIFIED Archive tree\n\n")
        print("\rVERIFIED Archive tree ")
    else:
        archiveTreeVerified = False
        logging.warning("FAILED Archive tree")
        vlogfile.write(datetime.datetime.today().strftime('[%Y-%m-%d %H:%M:%S] ') + "Warning: Verification failed for archive tree.\n\n")
        print("\rFAILED Archive tree   ")
        print("\nWarning: Verification failed for archive tree.")

    if archiveTreeVerified and archiveFilesVerified:
        logging.info("Verification successful")
        vlogfile.write("\n" + datetime.datetime.today().strftime('[%Y-%m-%d %H:%M:%S] ') + "Verification successful\n")
        print("\nVerification successful\n")
    else:
        status = 0
        if not archiveTreeVerified and not archiveFilesVerified:
            logging.warning("Verification failed due to both the archive tree and the following file(s) failing verification: %s",failedFiles.replace("\n","; "))
            vlogfile.write("\n" + datetime.datetime.today().strftime('[%Y-%m-%d %H:%M:%S] ') + "Verification failed due to both the archive tree and the following file(s) failing verification:\n\n")
            vlogfile.write(failedFiles + "\n")
            print("\nVerification failed due to both the archive tree and the following file(s) failing verification:\n")
            print(failedFiles)
        if not archiveTreeVerified:
            logging.warning("Verification failed due to the archive tree failing verification")
            vlogfile.write("\n" + datetime.datetime.today().strftime('[%Y-%m-%d %H:%M:%S] ') + "Verification failed due to the archive tree failing verification\n\n")
            print("\nVerification failed due to the archive tree failing verification\n")
        if not archiveFilesVerified:
            logging.warning("Verification failed due to the following file(s) failing verification: %s",failedFiles.replace("\n","; "))
            vlogfile.write("\n" + datetime.datetime.today().strftime('[%Y-%m-%d %H:%M:%S] ') + "Verification failed due to the following file(s) failing verification:\n\n")
            vlogfile.write(failedFiles + "\n")
            print("\nVerification failed due to the following file(s) failing verification:\n")
            print(failedFiles)
    
    vlogfile.close()
    if archivesettings["writeVerificationLog"]:
        with tarfile.open(archivesettings["archivefilepath"],"a",format=tarfile.GNU_FORMAT) as tar:
            tar.add(vlogfilepath,arcname=vlogfilename)
    
    os.remove(tempmanifestpath)
    if os.path.isdir(temppath):
        shutil.rmtree(temppath)
    if status == 2:
        status = 1
    return status
# !SECTION Functions
