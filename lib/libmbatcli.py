#                             MBAT
#               Copyright (C) 2021  Matt Hudson
#                   Email: matt@mjrhudson.uk

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

###########################################################################
#           TITLE: libmbatcli.py                                          #
#         AUTHORS: Matt Hudson (matt@mjrhudson.uk)                        #
# INITIAL RELEASE: 30-January-2021                                        #
#   LAST MODIFIED: 07-February-2021                                       #
###########################################################################

# SECTION Import Libraries
import os
import subprocess
import platform
import getpass
import logging
import json
import pathlib
# !SECTION Import Libraries

# SECTION Functions
def getVersionInfo():
    return "0.8.1"

def eolseq():
    return "\r\n" if platform.system() == "Windows" else "\n"

def clearScreen():
    os.system("cls" if platform.system() == "Windows" else "clear")

def getPassword(confirmPass):
    logging.debug("User entering password")
    runPassword = True
    while runPassword:
        password = getpass.getpass("Enter Archive Password: ")
        logging.debug("User entered password")
        if not confirmPass:
            print("")
            return password
        confirmpassword = getpass.getpass("Confirm Archive Password: ")
        logging.debug("User entered password confirmation")
        if password == confirmpassword:
            logging.debug("User password confirmation matched")
            print("")
            viewPassword = input("Would you like to view the password? (Y/N): ")
            if viewPassword.lower() == "y":
                logging.debug("User viewed password")
                print("\nPassword: " + password + "\n")
                input("Press enter to continue...")
                clearScreen()
            else:
                logging.debug("User did not view password")
                print("")
            return password
        else:
            logging.debug("User password confirmation did not match")
            print("\nError: Passwords do not match. Please try again.\n")

def loadSettings(rootpath):
    if platform.system() == "Windows":
        settingspath = rootpath + os.path.sep + "etc" + os.path.sep + "settings-win.json"
        if not os.path.isfile(settingspath):
            settings = {"defaultpaths": {"output": "$HOME\\Backups"}}
            with open(settingspath,"w") as f:
                f.write(json.dumps(settings,indent=4))
                f.write("\n")
            logging.info("Rebuilt settings-win.json")
            print("\nRebuilt settings-win.json")
    else:
        settingspath = rootpath + os.path.sep + "etc" + os.path.sep + "settings-unix.json"
        if not os.path.isfile(settingspath):
            settings = {"defaultpaths": {"output": "$HOME/Backups"}}
            with open(settingspath,"w") as f:
                f.write(json.dumps(settings,indent=4))
                f.write("\n")
            logging.info("Rebuilt settings-unix.json")
            print("\nRebuilt settings-unix.json")
    with open(settingspath,"r") as f:
        settings = json.load(f)
    defaultoutputpath = settings["defaultpaths"]["output"]
    defaultoutputpath = defaultoutputpath.replace("$HOME",str(pathlib.Path.home()))
    return defaultoutputpath

def loadProfile(rootpath,profilename):
    if platform.system() == "Windows":
        profilefilepath = rootpath + os.path.sep + "etc" + os.path.sep + "profiles-win.json"
    else:
        profilefilepath = rootpath + os.path.sep + "etc" + os.path.sep + "profiles-unix.json"
    if not os.path.isfile(profilefilepath):
        logging.error("Profiles file not found")
        print("\nError: Profiles file not found.\n")
        exit()
    with open(profilefilepath,"r") as f:
        profiles = json.load(f)
    if profilename.lower() in profiles:
        profile = profiles[profilename.lower()]
    else:
        logging.error("\"%s\" profile not found",profilename)
        print("\nError: \"" + profilename + "\" profile not found.\n")
        exit()
    for i in range(len(profile["dirs"])):
        if not os.path.isdir(profile["dirs"][i]):
            logging.error("\"%s\" does not lead to a folder",profile["dirs"][i])
            print("\nError: \"" + profile["dirs"][i] + "\" does not lead to a folder.\n")
            exit()
    return profile

def versionMeetsBase(verIn,verBase):
    verInList = verIn.split(".")
    verBaseList = verBase.split(".")
    if len(verInList) > len(verBaseList):
        verBaseList = verBaseList + (["0"]*(len(verInList)-len(verBaseList)))
    elif len(verBaseList) > len(verInList):
        verInList = verInList + (["0"]*(len(verBaseList)-len(verInList)))
    for i in range(len(verInList)):
        if int(verInList[i]) > int(verBaseList[i]):
            greaterThan = True
            break
        elif int(verInList[i]) < int(verBaseList[i]):
            greaterThan = False
            break
        if i == len(verInList) - 1:
            greaterThan = True
    return greaterThan

def getLibVersions():
    libVersions = {
        "gpg": {
            "ismin": False,
            "version": ""
        },
        "openssl": {
            "ismin": False,
            "version": ""
        }
    }
    try:
        gpgVersion = subprocess.check_output("gpg --version",shell=True).decode("utf-8").split(eolseq())[0].split("  ")[0]
        gpgVersionNum = gpgVersion.split(" ")[-1]
        gpgMinVersion = "2.2"
        libVersions["gpg"]["ismin"] = versionMeetsBase(gpgVersionNum,gpgMinVersion)
        libVersions["gpg"]["version"] = gpgVersion
    except:
        libVersions["gpg"]["version"] = "NULL"
    try:
        opensslVersion = subprocess.check_output("openssl version",shell=True).decode("utf-8").split(eolseq())[0].split("  ")[0]
        opensslVersionNum = opensslVersion.split(" ")[-1][:-1] # Probably should regexp this for ending with a letter rather than just assuming it does
        opensslMinVersion = "1.1.1"
        libVersions["openssl"]["ismin"] = versionMeetsBase(opensslVersionNum,opensslMinVersion)
        libVersions["openssl"]["version"] = opensslVersion
    except:
        libVersions["openssl"]["version"] = "NULL"
    return libVersions

def printManifestInfo(archiveFilepath,manifest):
    print("Information for \"" + archiveFilepath + "\"\n")
    print("System MBAT Version: ..................... " + manifest["info"][0])
    print("System Hostname: ......................... " + manifest["system"][0])
    print("System Platform/Version: ................. " + manifest["system"][1])
    print("Timestamp: ............................... %s-%s-%s %s:%s:%s" % (manifest["info"][1][0:4],manifest["info"][1][4:6],manifest["info"][1][6:8],manifest["info"][2][0:2],manifest["info"][2][2:4],manifest["info"][2][4:6]))
    print("Origin Path: ............................. " + manifest["tree"][0])
    print("CRC32 for Tree Paths: .................... " + manifest["tree"][1])
    print("CRC32 for Tree Data: ..................... " + manifest["tree"][2])
    print("Number of Files in Archive: .............. " + manifest["info"][4])
    print("Number of Empty Directories in Archive: .. " + manifest["info"][5])
    print("Verified on Creation?: ................... " + str("v" in manifest["info"][3]))
    print("Encrypted?: .............................. " + str("e" in manifest["info"][3]))
    print("Compressed?: ............................. " + str("z" in manifest["info"][3]))
    if versionMeetsBase(manifest["info"][0],"0.13.0"):
        print("Time Taken to Create Archive: ............ %sh %sm %ss" % (manifest["info"][6].split(":")[0],manifest["info"][6].split(":")[1],manifest["info"][6].split(":")[2]))
        print("Extra Information: ....................... " + manifest["info"][7])
    else:
        print("Extra Information: ....................... " + manifest["info"][6])
    print("")

def printHelpInfo():
    print("***************************************************************************")
    print("*                                                                         *")
    print("*                 MBAT - Matt\'s Backup and Archiving Tool                 *")
    print("*                                                                         *")
    print("***************************************************************************\n")
    print("First argument must be a mode specifier:\n")
    print("    -c Create  -v Verify  -x Extract  -i Information\n")
    print("Note: with the exception of -v, mode specifier flags can\'t be")
    print("      combined with each other.\n")
    print("Second argument must either be input paths or additional options.\n")
    print("Additional options:\n")
    print("      --out                   Specify custom output path (if option")
    print("                              not used, default output path will be")
    print("                              used as per <mbatrootpath>/etc/settings*\n")
    print("-p or --profile               Use profile\n")
    print("-e or --encrypt ENCLIBRARY    Use encryption (use without ENCLIBRARY")
    print("                              argument to use\n")
    print("                              the default encryption library)")
    print("-z or --compress              Use compression (not recommended if")
    print("                              using encryption)\n")
    print("      --masterpass PASSWORD   Use the same password for all archives")
    print("                              (use without PASSWORD")
    print("                              to be prompted for a password on")
    print("                              running)\n")
    print("Example usage: Create an archive of /path/to/dir in the default")
    print("               output location\n")
    print("    mbat -c /path/to/dir\n")
    print("Example usage: Create and verify an encrypted archive of /path/to/dir")
    print("               in /path/to/backup/dir\n")
    print("    mbat -cve --in /path/to/dir --out /path/to/backup/dir\n")
    print("Example usage: Create encrypted archives of /path/to/dir and")
    print("               /path/to/dir2 using the same password for both\n")
    print("    mbat -ce --in /path/to/dir /path/to/dir2 --masterpass\n")
    print("Example usage: Create and verify archives of directories according to the")
    print("               profile \'myprofile\' stored in <mbatrootpath>/etc as")
    print("               myprofile.json, using the same password for all archives\n")
    print("    mbat -pcv --in myprofile --masterpass\n")
    print("                            ----- END HELP -----                           \n")
# !SECTION Functions
