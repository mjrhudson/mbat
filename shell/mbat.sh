#!/bin/bash

#                             MBAT
#               Copyright (C) 2021  Matt Hudson
#                   Email: matt@mjrhudson.uk

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

###########################################################################
#           TITLE: mbat.sh                                                #
#         AUTHORS: Matt Hudson (matt@mjrhudson.uk)                        #
# INITIAL RELEASE: 23-November-2020                                       #
#   LAST MODIFIED: 28-January-2021                                        #
###########################################################################

# Place this shell script in any location for executables on
# the PATH, e.g ~/.local/bin (remove .sh from the end if preferred)

# Place the mbat root directory in ~/.local/scripts/mbat (or
# any location of your choice where you have write permissions
# - just be sure to modify mbatpath below)

mbatpath="$HOME/.local/scripts/mbat/mbatcli.py"
python3 -B "$mbatpath" "$@"
