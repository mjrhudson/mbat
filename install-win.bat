@echo off

REM                             MBAT
REM               Copyright (C) 2021  Matt Hudson
REM                   Email: matt@mjrhudson.uk

REM This program is free software: you can redistribute it and/or modify
REM it under the terms of the GNU General Public License as published by
REM the Free Software Foundation, either version 3 of the License, or
REM (at your option) any later version.

REM This program is distributed in the hope that it will be useful,
REM but WITHOUT ANY WARRANTY; without even the implied warranty of
REM MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
REM GNU General Public License for more details.

REM You should have received a copy of the GNU General Public License
REM along with this program.  If not, see <http://www.gnu.org/licenses/>.

REM #######################################################################
REM #           TITLE: install-win.bat                                    #
REM #         AUTHORS: Matt Hudson (matt@mjrhudson.uk)                    #
REM # INITIAL RELEASE: 30-November-2020                                   #
REM #   LAST MODIFIED: 05-April-2021                                      #
REM #######################################################################

REM Cleanup
echo.
echo ******************** MBAT Windows Install Script v0.1.4 *******************
echo.
echo|set /p="Cleaning up old versions of MBAT if present..."
if exist "%USERPROFILE%\.local\bin\mbat.bat" del "%USERPROFILE%\.local\bin\mbat.bat"
if exist "%USERPROFILE%\.local\scripts\mbat\libs" rmdir /s /q "%USERPROFILE%\.local\scripts\mbat\libs"
if exist "%USERPROFILE%\.local\scripts\mbat\lib" rmdir /s /q "%USERPROFILE%\.local\scripts\mbat\lib"
if exist "%USERPROFILE%\.local\scripts\mbat\logs" rmdir /s /q "%USERPROFILE%\.local\scripts\mbat\logs"
if exist "%USERPROFILE%\.local\scripts\mbat\shell" rmdir /s /q "%USERPROFILE%\.local\scripts\mbat\shell"
if exist "%USERPROFILE%\.local\scripts\mbat\etc\settings-win.json" del "%USERPROFILE%\.local\scripts\mbat\etc\settings-win.json"
if exist "%USERPROFILE%\.local\scripts\mbat\LICENCE" del "%USERPROFILE%\.local\scripts\mbat\LICENCE"
if exist "%USERPROFILE%\.local\scripts\mbat\README.md" del "%USERPROFILE%\.local\scripts\mbat\README.md"
if exist "%USERPROFILE%\.local\scripts\mbat\mbatcli.py" del "%USERPROFILE%\.local\scripts\mbat\mbatcli.py"
if exist "%USERPROFILE%\.local\scripts\mbat\runmbat.py" del "%USERPROFILE%\.local\scripts\mbat\runmbat.py"
echo Done
REM Make directories
echo|set /p="Creating MBAT directories..."
if not exist "%USERPROFILE%\.local\bin" mkdir "%USERPROFILE%\.local\bin"
if not exist "%USERPROFILE%\.local\scripts\mbat\etc" mkdir "%USERPROFILE%\.local\scripts\mbat\etc"
if not exist "%USERPROFILE%\.local\scripts\mbat\lib" mkdir "%USERPROFILE%\.local\scripts\mbat\lib"
if not exist "%USERPROFILE%\.local\scripts\mbat\logs" mkdir "%USERPROFILE%\.local\scripts\mbat\logs"
if not exist "%USERPROFILE%\.local\scripts\mbat\shell" mkdir "%USERPROFILE%\.local\scripts\mbat\shell"
echo Done
REM Copy files/folders
echo|set /p="Copying required files/folders..."
xcopy /s /y /i "%~dp0lib" "%USERPROFILE%\.local\scripts\mbat\lib" 1>NUL
xcopy /s /y /i "%~dp0shell" "%USERPROFILE%\.local\scripts\mbat\shell" 1>NUL
echo f | xcopy /s /y /f "%~dp0LICENCE" "%USERPROFILE%\.local\scripts\mbat\LICENCE" 1>NUL
echo f | xcopy /s /y /f "%~dp0README.md" "%USERPROFILE%\.local\scripts\mbat\README.md" 1>NUL
echo f | xcopy /s /y /f "%~dp0mbatcli.py" "%USERPROFILE%\.local\scripts\mbat\mbatcli.py" 1>NUL
echo Done
REM Create call script
echo|set /p="Creating call script in %USERPROFILE%\.local\bin (make sure this is on your PATH)..."
echo ^@echo off > "%USERPROFILE%\.local\bin\mbat.bat"
echo call %USERPROFILE%\.local\scripts\mbat\shell\mbat.bat %%* >> "%USERPROFILE%\.local\bin\mbat.bat"
echo Done
REM Show success message
echo.
echo ***************************************************************************
echo *                                                                         *
echo *                       MBAT installed successfully                       *
echo *                                                                         *
echo *   Note: If MBAT was already installed, settings have been reverted to   *
echo *                    default (profiles have been kept)                    *
echo *                                                                         *
echo ***************************************************************************
echo.
pause
echo.
